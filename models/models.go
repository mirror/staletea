// staletea
// Copyright (C) 2019 Jonas Franz
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package models

import (
	"gitea.com/jonasfranz/staletea/config"
	"github.com/go-xorm/core"
	"github.com/go-xorm/xorm"

	//use sqlite
	_ "github.com/mattn/go-sqlite3"
)

var x *xorm.Engine

// SetupDatabase will init a sqlite3 database and sync all models
func SetupDatabase() {
	var err error
	x, err = xorm.NewEngine("sqlite3", config.DatabasePath.Get().(string))
	if err != nil {
		panic(err)
	}
	x.SetMapper(core.GonicMapper{})
	syncModels()
}

func syncModels() {
	_ = x.Sync2(new(Issue))
	_ = x.Sync2(new(Repository))
}
